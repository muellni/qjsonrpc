#ifndef QJSONRPCSERVERPRIVATE_H
#define QJSONRPCSERVERPRIVATE_H

#include <QList>

#if QT_VERSION >= 0x050000
#include <QJsonDocument>
#else
#include "json/qjsondocument.h"
#endif

#include "qjsonrpcsocket.h"

class QJsonRpcServerPrivate
{
public:
#if QT_VERSION >= 0x050100 || QT_VERSION <= 0x050000
    QJsonDocument::JsonFormat format;
    QJsonRpcServerPrivate() : format(QJsonDocument::Compact) {}
#else
    QJsonRpcServerPrivate() {}
#endif

    QList<QJsonRpcSocket*> clients;

};

#endif // QJSONRPCSERVERPRIVATE_H
