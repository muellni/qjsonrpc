#include "qjsonrpcserver.h"

#include <QDebug>

#include "qjsonrpcserverprivate.h"
#include "qjsonrpcmessage.h"
#include "qjsonrpcservice.h"
#include "qjsonrpcserviceprovider.h"

QJsonRpcServer::QJsonRpcServer(QJsonRpcServerPrivate *dd, QObject *parent)
    : QObject(parent),
      d_ptr(dd)
{
}

QJsonRpcServer::~QJsonRpcServer()
{
    Q_D(QJsonRpcServer);
     foreach (QJsonRpcSocket *client, d->clients)
        client->deleteLater();
    d->clients.clear();
}

void QJsonRpcServer::addService(QJsonRpcService *service)
{
    QJsonRpcServiceProvider::addService(service);
    connect(service, SIGNAL(notifyConnectedClients(QJsonRpcMessage)),
               this, SLOT(notifyConnectedClients(QJsonRpcMessage)));
    connect(service, SIGNAL(notifyConnectedClients(QString,QVariantList)),
               this, SLOT(notifyConnectedClients(QString,QVariantList)));
}

#if QT_VERSION >= 0x050100 || QT_VERSION <= 0x050000
QJsonDocument::JsonFormat QJsonRpcServer::wireFormat() const
{
    Q_D(const QJsonRpcServer);
    return d->format;
}

void QJsonRpcServer::setWireFormat(QJsonDocument::JsonFormat format)
{
    Q_D(QJsonRpcServer);
    d->format = format;
}
#endif

void QJsonRpcServer::notifyConnectedClients(const QString &method, const QVariantList &params)
{
    QJsonRpcMessage notification = QJsonRpcMessage::createNotification(method, params);
    notifyConnectedClients(notification);
}

void QJsonRpcServer::notifyConnectedClients(const QJsonRpcMessage &message)
{
    Q_D(QJsonRpcServer);
    for (int i = 0; i < d->clients.size(); ++i)
        d->clients[i]->notify(message);
}

void QJsonRpcServer::processMessage(const QJsonRpcMessage &message)
{
    QJsonRpcSocket *socket = static_cast<QJsonRpcSocket*>(sender());
    if (!socket) {
        qDebug() << Q_FUNC_INFO << "called without service socket";
        return;
    }

    QJsonRpcServiceProvider::processMessage(socket, message);
}

