#ifndef QJSONRPCSERVER_H
#define QJSONRPCSERVER_H

#include <QObject>
#include <QVariantList>
#include <QScopedPointer>

#if QT_VERSION >= 0x050000
#include <QJsonDocument>
#else
#include "json/qjsondocument.h"
#endif

#include "qjsonrpcserviceprovider.h"

#include "qjson_export.h"

QT_BEGIN_NAMESPACE
class QString;
QT_END_NAMESPACE

class QJsonRpcServerPrivate;
class QJsonRpcService;
class QJSONRPC_EXPORT QJsonRpcServer : public QObject,
                                       public QJsonRpcServiceProvider
{
    Q_OBJECT
public:
    virtual ~QJsonRpcServer();
    virtual QString errorString() const = 0;
    virtual void addService(QJsonRpcService *service);

#if QT_VERSION >= 0x050100 || QT_VERSION <= 0x050000
    QJsonDocument::JsonFormat wireFormat() const;
    void setWireFormat(QJsonDocument::JsonFormat format);
#endif

public Q_SLOTS:
    void notifyConnectedClients(const QJsonRpcMessage &message);
    void notifyConnectedClients(const QString &method, const QVariantList &params = QVariantList());

protected Q_SLOTS:
    virtual void processIncomingConnection() = 0;
    virtual void clientDisconnected() = 0;
    void processMessage(const QJsonRpcMessage &message);

protected:
    explicit QJsonRpcServer(QJsonRpcServerPrivate *dd, QObject *parent);
    Q_DECLARE_PRIVATE(QJsonRpcServer)
    QScopedPointer<QJsonRpcServerPrivate> d_ptr;

};


#endif // QJSONRPCSERVER_H
