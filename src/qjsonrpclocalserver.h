#ifndef QJSONRPCLOCALSERVER_H
#define QJSONRPCLOCALSERVER_H

#include "qjsonrpcserver.h"

#include "qjson_export.h"

QT_BEGIN_NAMESPACE
class QString;
QT_END_NAMESPACE


class QJsonRpcLocalServerPrivate;
class QJSONRPC_EXPORT QJsonRpcLocalServer : public QJsonRpcServer
{
    Q_OBJECT
public:
    explicit QJsonRpcLocalServer(QObject *parent = 0);
    ~QJsonRpcLocalServer();

    QString errorString() const;
    bool listen(const QString &service);

private Q_SLOTS:
    void processIncomingConnection();
    void clientDisconnected();

private:
    Q_DECLARE_PRIVATE(QJsonRpcLocalServer)

};

#endif // QJSONRPCLOCALSERVER_H
