#ifndef QJSONRPCTCPSERVER_H
#define QJSONRPCTCPSERVER_H

#include "qjsonrpcserver.h"

#include "qjson_export.h"

QT_BEGIN_NAMESPACE
class QString;
class QHostAddress;
class QObject;
QT_END_NAMESPACE

class QJsonRpcTcpServerPrivate;
class QJSONRPC_EXPORT QJsonRpcTcpServer : public QJsonRpcServer
{
    Q_OBJECT
public:
    explicit QJsonRpcTcpServer(QObject *parent = 0);
    ~QJsonRpcTcpServer();

    QString errorString() const;
    bool listen(const QHostAddress &address, quint16 port);

private Q_SLOTS:
    void processIncomingConnection();
    void clientDisconnected();

private:
    Q_DECLARE_PRIVATE(QJsonRpcTcpServer)
};

#endif // QJSONRPCTCPSERVER_H
