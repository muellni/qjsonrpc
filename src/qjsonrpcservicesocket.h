#ifndef QJSONRPCSERVICESOCKET_H
#define QJSONRPCSERVICESOCKET_H

#include <QtGlobal>

#include "qjsonrpcsocket.h"
#include "qjsonrpcserviceprovider.h"

#include "qjson_export.h"

QT_BEGIN_NAMESPACE
class QIODevice;
class QObject;
QT_END_NAMESPACE

class QJsonRpcMessage;
class QJSONRPC_EXPORT QJsonRpcServiceSocket : public QJsonRpcSocket,
                                              public QJsonRpcServiceProvider
{
    Q_OBJECT
public:
    explicit QJsonRpcServiceSocket(QIODevice *device, QObject *parent = 0);
    ~QJsonRpcServiceSocket();

private:
    virtual void processRequestMessage(const QJsonRpcMessage &message);

};

#endif // QJSONRPCSERVICESOCKET_H
