#ifndef QJSONRPCSERVICEPROVIDER_H
#define QJSONRPCSERVICEPROVIDER_H

#include <QtGlobal>
#include <QScopedPointer>

#include "qjsonrpc_export.h"

class QJsonRpcService;
class QJsonRpcSocket;
class QJsonRpcMessage;
class QJsonRpcServiceProviderPrivate;
class QJSONRPC_EXPORT QJsonRpcServiceProvider
{
public:
    ~QJsonRpcServiceProvider();
    virtual void addService(QJsonRpcService *service);

protected:
    QJsonRpcServiceProvider();
    void processMessage(QJsonRpcSocket *socket, const QJsonRpcMessage &message);

private:
    Q_DECLARE_PRIVATE(QJsonRpcServiceProvider)
    QScopedPointer<QJsonRpcServiceProviderPrivate> d_ptr;

};

#endif // QJSONRPCSERVICEPROVIDER_H
