#include "qjsonrpcserviceprovider.h"

#include <QObjectCleanupHandler>
#include <QMetaClassInfo>
#include <QDebug>

#include "qjsonrpcservice.h"
#include "qjsonrpcserviceprivate.h"
#include "qjsonrpcmessage.h"
#include "qjsonrpcsocket.h"

class QJsonRpcServiceProviderPrivate
{
public:
    QHash<QString, QJsonRpcService*> services;
    QObjectCleanupHandler cleanupHandler;

};

QJsonRpcServiceProvider::QJsonRpcServiceProvider()
    : d_ptr(new QJsonRpcServiceProviderPrivate)
{
}

QJsonRpcServiceProvider::~QJsonRpcServiceProvider()
{
}

void QJsonRpcServiceProvider::addService(QJsonRpcService *service)
{
    Q_D(QJsonRpcServiceProvider);
    const QMetaObject *mo = service->metaObject();
    for (int i = 0; i < mo->classInfoCount(); i++) {
        const QMetaClassInfo mci = mo->classInfo(i);
        if (mci.name() == QLatin1String("serviceName")) {
            service->d_ptr->cacheInvokableInfo();
            d->services.insert(mci.value(), service);
            if (!service->parent())
                d->cleanupHandler.add(service);

            return;
        }
    }

    qDebug() << Q_FUNC_INFO << "service added without serviceName classinfo, aborting";
}

void QJsonRpcServiceProvider::processMessage(QJsonRpcSocket *socket, const QJsonRpcMessage &message)
{
    Q_D(QJsonRpcServiceProvider);
    switch (message.type()) {
        case QJsonRpcMessage::Request:
        case QJsonRpcMessage::Notification: {
            QString serviceName = message.method().section(".", 0, -2);
            if (serviceName.isEmpty() || !d->services.contains(serviceName)) {
                if (message.type() == QJsonRpcMessage::Request) {
                    QJsonRpcMessage error = message.createErrorResponse(QJsonRpc::MethodNotFound,
                                                                        QString("service '%1' not found").arg(serviceName));
                    socket->notify(error);
                }
            } else {
                QJsonRpcService *service = d->services.value(serviceName);
                service->d_ptr->socket = socket;
                if (message.type() == QJsonRpcMessage::Request)
                    QObject::connect(service, SIGNAL(result(QJsonRpcMessage)), socket, SLOT(notify(QJsonRpcMessage)));
                service->dispatch(message);
            }
        }
        break;

        case QJsonRpcMessage::Response:
            // we don't handle responses in the provider
            break;

        default: {
            QJsonRpcMessage error = message.createErrorResponse(QJsonRpc::InvalidRequest,
                                                                QString("invalid request"));
            socket->notify(error);
            break;
        }
    };
}
